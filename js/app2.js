function consulta() {
    const http = new XMLHttpRequest();
    const url = "https://jsonplaceholder.typicode.com/users";
    const txtId = document.getElementById("txtId").value;
    const res = document.getElementById("lista");
    const body = document.body;

    // Limpiar el contenido previo de la tabla y quitar la clase de fondo
    res.innerHTML = "<tbody>";
    body.classList.remove("fondo-no-encontrado");

    // Validar la respuesta
    http.onreadystatechange = function () {
        if (this.status == 200 && this.readyState == 4) {
            // Aquí se dibuja la página
            const json = JSON.parse(this.responseText);

            // Variable para verificar si se encontró la ID
            let idEncontrada = false;

            // Ciclo para ir tomando cada uno de los registros
            for (const datos of json) {
                // Verificar si la ID coincide con la buscada
                if (datos.id == txtId) {
                    res.innerHTML += '<tr> <td class ="columna">'+datos.id + '</td>'
                    + '<td class ="columna">'+datos.name + '</td>'
                    + '<td class ="columna">'+datos.username + '</td>' 
                    + '<td class ="columna">'+datos.email + '</td>'
                    + '<td class ="columna">'+datos.address.street + '</td>'
                    + '<td class ="columna">'+datos.address.suite + '</td>'
                    + '<td class ="columna">'+datos.address.city + '</td>'
                    + '<td class ="columna">'+datos.address.zipcode + '</td>'
                    + '<td class ="columna">'+datos.address.geo.lat + '</td>'
                    + '<td class ="columna">'+datos.address.geo.lng + '</td> </tr>'
                    idEncontrada = true;
                    break; // Termina el bucle una vez que se encuentra la ID
                }
            }

            // Si no se encontró la ID, agregar la clase de fondo
            if (!idEncontrada) {
                res.innerHTML += '<tr><td colspan="3">ID no encontrada</td></tr>';
                body.classList.add("fondo-no-encontrado");
            }

            res.innerHTML += "</tbody>";
        }
    };

    http.open('GET', url, true);
    http.send();
}

function cargarDatos(){
    const http = new XMLHttpRequest;
    const url = "https://jsonplaceholder.typicode.com/users";

    //validar la respuesta
    http.onreadystatechange = function(){
        if(this.status ==200 && this.readyState== 4){
            //aqui se dibuja la pagina
            let res = document.getElementById("lista");
            const json = JSON.parse(this.responseText);

            //Ciclo para ir tomando cada uno de los registros

            for(const datos of json){
                res.innerHTML += '<tr> <td class ="columna">'+datos.id + '</td>'
                    + '<td class ="columna">'+datos.name + '</td>'
                    + '<td class ="columna">'+datos.username + '</td>' 
                    + '<td class ="columna">'+datos.email + '</td>'
                    + '<td class ="columna">'+datos.address.street + '</td>'
                    + '<td class ="columna">'+datos.address.suite + '</td>'
                    + '<td class ="columna">'+datos.address.city + '</td>'
                    + '<td class ="columna">'+datos.address.zipcode + '</td>'
                    + '<td class ="columna">'+datos.address.geo.lat + '</td>'
                    + '<td class ="columna">'+datos.address.geo.lng + '</td> </tr>'
            }
            res.innerHTML +="</tbody>"
        }
    }
    http.open('GET',url,true);
    http.send();
}

// Codificar el botón de búsqueda
document.getElementById("btnBuscar").addEventListener("click", function () {
    consulta();
});

document.getElementById("btnCargar").addEventListener("click", function () {
    cargarDatos();
});

